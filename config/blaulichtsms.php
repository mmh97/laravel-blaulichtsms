<?php

return [
    "customer_id"        => env("BLAULICHTSMS_CUSTOMER_ID"),
    "dashboard_username" => env("BLAULICHTSMS_DASHBOARD_USERNAME"),
    "dashboard_password" => env("BLAULICHTSMS_DASHBOARD_PASSWORD"),
    "username"           => env("BLAULICHTSMS_USERNAME"),
    "password"           => env("BLAULICHTSMS_PASSWORD"),
];
